﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosDoor : MonoBehaviour
{
    public Color color = Color.white;
    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawCube(this.transform.position, new Vector3(119, 122, 0));
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;


[CustomEditor(typeof(Grid))]
public class GridEditor : Editor
{
    private Grid grid;

    private int oldIndex = 0;


    private void OnEnable()
    {
        grid = (Grid)target;
    }

    [MenuItem("Assets/Create/TileSet")]
    static void CreateTileSet()
    {
        var asset = ScriptableObject.CreateInstance<TileSet>();
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);
        
        if(string.IsNullOrEmpty(path))
        {
            path = "Assets";
        }
        else if(Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(path), "");
        }
        else
        {
            path += "/";
        }

        var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "TileSet.asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        asset.hideFlags = HideFlags.DontSave;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        grid.width = CreateSlider("Width", grid.width);
        grid.height = CreateSlider("Height", grid.height);

        if(GUILayout.Button("Open Grid Window"))
        {
            GridWindow window = (GridWindow)EditorWindow.GetWindow(typeof(GridWindow));
            window.init();
        }

        #region Tile Prefab
        EditorGUI.BeginChangeCheck();
        var newTilePrefab = (Transform)EditorGUILayout.ObjectField("Tile Prefab", grid.tilePrefab, typeof(Transform), false);
        if(EditorGUI.EndChangeCheck())
        {
            grid.tilePrefab = newTilePrefab;
            Undo.RecordObject(target, "Grid Changed");
        }
        #endregion

        #region TileMap

        EditorGUI.BeginChangeCheck();
        var newTileSet = (TileSet)EditorGUILayout.ObjectField("Tileset", grid.tileSet, typeof(TileSet), false);
        if(EditorGUI.EndChangeCheck())
        {
            grid.tileSet = newTileSet;
            Undo.RecordObject(target, "Grid Changed");
        }

        if(grid.tileSet != null)
        {
            EditorGUI.BeginChangeCheck();
            var names = new string[grid.tileSet.prefab.Length];
            var values = new int[names.Length];

            for(int i = 0; i <names.Length; i++)
            {
                names[i] = grid.tileSet.prefab[i] != null ? grid.tileSet.prefab[i].name : "";
                values[i] = i;
            }

            var index = EditorGUILayout.IntPopup("Select Tile", oldIndex, names, values);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                if(oldIndex != index)
                {
                    oldIndex = index;
                    grid.tilePrefab = grid.tileSet.prefab[index];

                    float width = grid.tilePrefab.GetComponent<Renderer>().bounds.size.x;
                    float height = grid.tilePrefab.GetComponent<Renderer>().bounds.size.y;

                    //grid.width = width;
                    //grid.height = height;
                }
            }
        }
        #endregion

    }

    private float CreateSlider(string labelName, float sliderPosition)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Grid" + labelName);
        sliderPosition = EditorGUILayout.Slider(sliderPosition, 1f, 1000f, null);
        GUILayout.EndHorizontal();

        return sliderPosition;
    }


    private void OnSceneGUI()
    {
        int controlld = GUIUtility.GetControlID(FocusType.Passive);
        Event e = Event.current;
        Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
        Vector3 mousePos = ray.origin;

        #region Create

        if (e.isMouse && e.type == EventType.MouseDown && e.button == 0)
        {
            GUIUtility.hotControl = controlld;
            e.Use();

            GameObject gameObj;

            Transform prefab = grid.tilePrefab;

            if(prefab)
            {
                Undo.IncrementCurrentGroup();
                
                Vector3 aligned = new Vector3(Mathf.Floor(mousePos.x/grid.width) * grid.width + grid.width/2.0f, Mathf.Floor(mousePos.y / grid.height) * grid.height + grid.height / 2.0f, 0.0f);
                if(prefab.tag == "Door")
                {
                    if (GetTransformFromPosition(aligned).gameObject.tag == "Snap")
                    {
                        DestroyImmediate(GetTransformFromPosition(aligned).gameObject);
                        gameObj = (GameObject)PrefabUtility.InstantiatePrefab(prefab.gameObject);
                        gameObj.transform.position = aligned;
                        gameObj.transform.parent = grid.transform;
                        Undo.RegisterCreatedObjectUndo(gameObj, "Create" + gameObj.name);
                        return;
                    }
                }
                else
                {
                    if (GetTransformFromPosition(aligned) != null)
                    {
                        return;
                    }

                    gameObj = (GameObject)PrefabUtility.InstantiatePrefab(prefab.gameObject);
                    gameObj.transform.position = aligned;
                    gameObj.transform.parent = grid.transform;
                    Undo.RegisterCreatedObjectUndo(gameObj, "Create" + gameObj.name);
                }
                
                
                
            }
        }

        #endregion

        #region Destroy
        if (e.isMouse && e.type == EventType.MouseDown && e.button == 1)
        {
            GUIUtility.hotControl = controlld;
            e.Use();
            Vector3 Aligned = new Vector3(Mathf.Floor(mousePos.x / grid.width) * grid.width + grid.width / 2.0f, Mathf.Floor(mousePos.y / grid.height) * grid.height + grid.height / 2.0f, 0.0f);
            Transform trans = GetTransformFromPosition(Aligned);
            if(trans != null)
            {
                DestroyImmediate(trans.gameObject);
            }
        }
        #endregion

        if (e.isMouse && e.type == EventType.MouseUp)
        {
            GUIUtility.hotControl = 0;
        }
    }

    Transform GetTransformFromPosition(Vector3 aligned)
    {
        int i = 0;
        while(i < grid.transform.childCount)
        {
            Transform trans = grid.transform.GetChild(i);
            if(trans.position == aligned)
            {
                return trans;
            }

            i++;
        }

        return null;
    }

}
